import { Injectable } from '@angular/core';
import {UserSettingsModel} from './user-settings.model';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserSettingsService {

  url = 'https://putsreq.com/ckuiUwn6gkOCzC8JcsRL';

  constructor(private http: HttpClient) { }

  post(userSettings: UserSettingsModel): Observable<UserSettingsModel> {
    return this.http.post<UserSettingsModel>(this.url, userSettings);
  }
}
