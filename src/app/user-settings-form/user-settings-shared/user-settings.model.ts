export interface UserSettingsModel {
  name: string;
  emailOffers: boolean;
  interfaceStyle: string;
  subscriptionType: number;
  notes: string;
}
