import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  constructor() { }

  getAll(): Observable<Object[]> {
    return of([
      {
        value: 1,
        label: 'Monthly'
      },
      {
        value: 2,
        label: 'Annual'
      }]);
  }
}
