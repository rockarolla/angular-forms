import { Component, OnInit } from '@angular/core';
import {UserSettingsModel} from './user-settings-shared/user-settings.model';
import {NgForm, NgModel} from '@angular/forms';
import {UserSettingsService} from './user-settings-shared/user-settings.service';
import {SubscriptionService} from './user-settings-shared/subscription.service';

@Component({
  selector: 'app-user-settings-form',
  templateUrl: './user-settings-form.component.html',
  styleUrls: ['./user-settings-form.component.css']
})
export class UserSettingsFormComponent implements OnInit {

  originalUserSettings: UserSettingsModel = {
    name: null,
    emailOffers: null,
    interfaceStyle: null,
    subscriptionType: null,
    notes: null
  };
  userSettings: UserSettingsModel = {...this.originalUserSettings};
  postError = false;
  postErrorMessage = '';
  subscriptionTypes: any;

  singleModel = '1';
  startDate: any = new Date();

  constructor(private userSettingsService: UserSettingsService, private subscriptionService: SubscriptionService) { }

  ngOnInit() {
    this.subscriptionTypes = this.subscriptionService.getAll();
  }

  onSubmit(form: NgForm) {
    console.log('submiting form: ', form.valid);

    if (form.valid) {
      this.userSettingsService.post(this.userSettings).subscribe(
        result => console.log('success response: ', result),
        error => this.onHttpError(error)
      );
      this.postError = false;
    } else {
      this.postError = true;
      this.postErrorMessage = 'Review for fields errors';
    }
  }

  onBlur(element: NgModel) {
    console.log('blurring ', element.valid);
  }

  private onHttpError(errorResponse: any) {
    console.log('error response: ', errorResponse);
    this.postError = true;
    this.postErrorMessage = errorResponse.error;
  }
}
